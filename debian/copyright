Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pulseaudio-qt
Source: https://invent.kde.org/libraries/pulseaudio-qt
Upstream-Contact: kde-devel@kde.org

Files: *
Copyright: 2016-2018 David Rosca <nowrep@gmail.com>
    2014, 2015 Harald Sitter <sitter@kde.org>
    2016, 2018 Nicolas Fella <nicolas.fella@gmx.de>
License: LGPL-2.1 or LGPL-3 or KDEeV

Files: debian/*
Copyright: 2019, Scarlett Moore <sgmoore@kde.org>
           2020-2021, 2024, Pino Toscano <pino@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: KDEeV
 Any later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
